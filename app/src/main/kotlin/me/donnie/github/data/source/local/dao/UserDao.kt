package me.donnie.github.data.source.local.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import me.donnie.github.data.entity.User

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
@Dao
abstract class UserDao {

    @Query("SELECT * FROM user")
    abstract fun getUser(): Flowable<List<User>>

    @Query("DELETE FROM user")
    abstract fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(user: User)

    @Transaction
    open fun clearAndInsert(user: User) {
        deleteAll()
        insert(user)
    }
}