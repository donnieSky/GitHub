package me.donnie.github.common.di.module

import dagger.Module
import dagger.Provides
import me.donnie.github.common.di.qualifier.Auth
import me.donnie.github.common.di.qualifier.GitHub
import me.donnie.github.data.source.api.AuthApi
import me.donnie.github.data.source.api.UserApi
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideAuthApi(@Auth retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)

    @Singleton
    @Provides
    fun provideUserApi(@GitHub retrofit: Retrofit): UserApi = retrofit.create(UserApi::class.java)

}