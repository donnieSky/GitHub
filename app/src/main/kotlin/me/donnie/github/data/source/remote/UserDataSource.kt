package me.donnie.github.data.source.remote

import io.reactivex.Single
import me.donnie.github.data.entity.Auth
import me.donnie.github.data.entity.Token
import me.donnie.github.data.entity.User

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
interface UserDataSource {

    fun login(auth: Auth): Single<Token>

    fun getUser(): Single<User>

}