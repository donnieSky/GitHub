/**
 * @author donnieSky
 * @created_at 2018/5/11.
 * @description
 * @version
 */

object Libraries {

    const val gradle = "com.android.tools.build:gradle:${Versions.gradle}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
    const val reflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"

    object Arouter {
        const val api = "com.alibaba:arouter-api:1.3.1"
        const val compiler = "com.alibaba:arouter-compiler:1.1.4"
    }

    object Support {
        const val support_v4 = "com.android.support:support-v4:${Versions.support}"
        const val appcompat_v7 = "com.android.support:appcompat-v7:${Versions.support}"
        const val design = "com.android.support:design:${Versions.support}"
        const val cardview_v7 = "com.android.support:cardview-v7:${Versions.support}"
        const val customtabs = "com.android.support:customtabs:${Versions.support}"
        const val constraint = "com.android.support.constraint:constraint-layout:${Versions.constrain}"
        const val multidex = "com.android.support:multidex:${Versions.multidex}"
        const val emoji = "com.android.support:support-emoji-appcompat:${Versions.support}"
        const val pref_v7 = "com.android.support:preference-v7:${Versions.support}"
        const val pref_v14 = "com.android.support:preference-v14:${Versions.support}"

        object Test {
            val runner = "com.android.support.test:runner:${Versions.runner}"
            val espresso = "com.android.support.test.espresso:espresso-core:${Versions.espresso}"
            val contrib = "com.android.support.test.espresso:espresso-contrib:${Versions.espresso}"
        }
    }

    object Stetho {
        const val core = "com.facebook.stetho:stetho:${Versions.stetho}"
        const val okhttp3 = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho}"
        const val urlconnection = "com.facebook.stetho:stetho-urlconnection:${Versions.stetho}"
    }

    const val ktx = "androidx.core:core-ktx:${Versions.ktx}"

    const val junit = "junit:junit:${Versions.junit}"

    const val react_native = "com.facebook.react:react-native:${Versions.react_native}"
    const val compiler = "com.android.databinding:compiler:${Versions.databinding}"

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    object Kotpref {
        const val core = "com.chibatching.kotpref:kotpref:${Versions.kotpref}"
        const val initializer = "com.chibatching.kotpref:initializer:${Versions.kotpref}"
        const val enum_support = "com.chibatching.kotpref:enum-support:${Versions.kotpref}"
    }

    object Glide {
        const val core = "com.github.bumptech.glide:glide:${Versions.glide}"
        const val okhttp3 = "com.github.bumptech.glide:okhttp3-integration:${Versions.glide}"
        const val compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    }

    object LifeCycle {
        val runtime = "android.arch.lifecycle:runtime:${Versions.lifecycle}"
        val extensions = "android.arch.lifecycle:extensions:${Versions.lifecycle}"
        val reactivestreams = "android.arch.lifecycle:reactivestreams:${Versions.lifecycle}"
    }

    object Room {
        val runtime = "android.arch.persistence.room:runtime:${Versions.room}"
        val rxjava2 = "android.arch.persistence.room:rxjava2:${Versions.room}"
        val compiler = "android.arch.persistence.room:compiler:${Versions.room}"
    }

    object Dagger2 {
        const val core = "com.google.dagger:dagger:${Versions.dagger2}"
        const val compiler = "com.google.dagger:dagger-compiler:${Versions.dagger2}"
        object Android {
            const val core = "com.google.dagger:dagger-android:${Versions.dagger2}"
            const val support = "com.google.dagger:dagger-android-support:${Versions.dagger2}"
            const val processor = "com.google.dagger:dagger-android-processor:${Versions.dagger2}"
        }
    }

    object Retrofit {
        const val core = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"

        object Adapter {
            const val rxjava2 = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
            const val java8 = "com.squareup.retrofit2:adapter-java8:${Versions.retrofit}"
            const val guava = "com.squareup.retrofit2:adapter-guava:${Versions.retrofit}"
            const val scala = "com.squareup.retrofit2:adapter-scala:${Versions.retrofit}"
        }

        object Converter {
            const val gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
            const val jackson = "com.squareup.retrofit2:converter-jackson:${Versions.retrofit}"
            const val moshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
            const val protobuf = "com.squareup.retrofit2:converter-protobuf:${Versions.retrofit}"
        }
    }

    object OkHttp {
        const val core = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
        const val logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    }

    object RxJava2 {
        val core = "io.reactivex.rxjava2:rxjava:${Versions.rxJava2}"
        val android = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
        val kotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"
    }

}