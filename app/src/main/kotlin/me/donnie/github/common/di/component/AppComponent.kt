package me.donnie.github.common.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import me.donnie.github.common.App
import me.donnie.github.common.di.module.ApiModule
import me.donnie.github.common.di.module.AppModule
import me.donnie.github.common.di.module.DaoModule
import javax.inject.Singleton

/**
 * @author donnieSky
 * @created_at 2018/5/16.
 * @description
 * @version
 */
@Singleton
@Component(modules = [
    AppModule::class,
    ApiModule::class,
    DaoModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)
}