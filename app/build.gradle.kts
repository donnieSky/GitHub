import org.gradle.api.JavaVersion.VERSION_1_8

plugins {
    id("com.android.application") version Versions.gradle
    kotlin("android") version Versions.kotlin
    kotlin("kapt") version Versions.kotlin
    kotlin("android.extensions") version Versions.kotlin
}

android {
    compileSdkVersion(Versions.compileSdk)
    dataBinding.isEnabled = true

    defaultConfig {
        applicationId = Versions.applicationId
        minSdkVersion(Versions.minSdk)
        targetSdkVersion(Versions.targetSdk)
        versionCode = Versions.major * 1000 + Versions.minor * 10 + Versions.patch
        versionName = "${Versions.major}.${Versions.minor}.${Versions.patch}"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        flavorDimensions("${Versions.major}.${Versions.minor}.${Versions.patch}")
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
        resConfigs("en", "zh-rCN", "hdpi")

        javaCompileOptions {
            annotationProcessorOptions {
                arguments = mapOf("room.schemaLocation" to "$projectDir/schemas")
            }
        }
    }

    kapt {
        useBuildCache = true
        correctErrorTypes = true
        generateStubs = true
    }

    lintOptions {
        isAbortOnError = false
    }

    java {
        sourceCompatibility = VERSION_1_8
        targetCompatibility = VERSION_1_8
    }

    sourceSets {
        getByName("main") {
            java.srcDirs("src/main/kotlin")
            jniLibs.srcDirs("libs")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libraries.Support.support_v4)
    implementation(Libraries.Support.appcompat_v7)
    implementation(Libraries.Support.design)
    implementation(Libraries.Support.cardview_v7)
    implementation(Libraries.Support.customtabs)
    implementation(Libraries.Support.multidex)
    implementation(Libraries.Support.emoji)
    implementation(Libraries.Support.pref_v7)
    implementation(Libraries.Support.pref_v14)
    implementation(Libraries.Support.constraint)

    kapt(Libraries.compiler)

    implementation(Libraries.stdlib)
    implementation(Libraries.reflect)
    implementation(Libraries.ktx)

    implementation(Libraries.Retrofit.core)
    implementation(Libraries.Retrofit.Adapter.rxjava2)
    implementation(Libraries.Retrofit.Converter.gson)
    implementation(Libraries.Retrofit.Converter.moshi)
    implementation(Libraries.OkHttp.logging)

    implementation(Libraries.LifeCycle.runtime)
    implementation(Libraries.LifeCycle.extensions)
    implementation(Libraries.LifeCycle.reactivestreams)
    implementation(Libraries.Room.runtime)
    implementation(Libraries.Room.rxjava2)
    kapt(Libraries.Room.compiler)

    implementation(Libraries.RxJava2.core)
    implementation(Libraries.RxJava2.android)
    implementation(Libraries.RxJava2.kotlin)

    implementation(Libraries.Dagger2.core)
    implementation(Libraries.Dagger2.Android.core)
    implementation(Libraries.Dagger2.Android.support)
    kapt(Libraries.Dagger2.compiler)
    kapt(Libraries.Dagger2.Android.processor)

    implementation(Libraries.Glide.core)
    implementation(Libraries.Glide.okhttp3)
    kapt(Libraries.Glide.compiler)

    implementation(Libraries.timber)

    implementation(Libraries.Kotpref.core)
    implementation(Libraries.Kotpref.initializer)
    implementation(Libraries.Kotpref.enum_support)

    debugImplementation(Libraries.Stetho.core)
    debugImplementation(Libraries.Stetho.okhttp3)

    testImplementation(Libraries.junit)
    androidTestImplementation(Libraries.Support.Test.runner)
    androidTestImplementation(Libraries.Support.Test.espresso)
}