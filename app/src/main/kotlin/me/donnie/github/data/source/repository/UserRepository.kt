package me.donnie.github.data.source.repository

import io.reactivex.Single
import me.donnie.github.data.entity.Auth
import me.donnie.github.data.entity.Token
import me.donnie.github.data.entity.User
import me.donnie.github.data.source.api.UserApi
import me.donnie.github.data.source.local.dao.UserDao
import me.donnie.github.data.source.remote.UserDataSource
import javax.inject.Inject

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
class UserRepository @Inject constructor(private val userApi: UserApi,
                                         private val userDao: UserDao) : UserDataSource {
    override fun login(auth: Auth): Single<Token> {
        return userApi.login(auth)
    }

    override fun getUser(): Single<User> {
        return userApi.getUser()
    }
}