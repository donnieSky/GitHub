package me.donnie.github.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import me.donnie.github.data.entity.User
import me.donnie.github.data.source.local.dao.UserDao

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
@Database(entities = [User::class], version = 1)
abstract class DB : RoomDatabase() {

    abstract fun userDao(): UserDao

}