package me.donnie.github.common.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerAppCompatActivity

/**
 * @author donnieSky
 * @created_at 2018/5/16.
 * @description
 * @version
 */
@Subcomponent(modules = [AndroidInjectionModule::class])
interface ActivitySubComponent : AndroidInjector<DaggerAppCompatActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<DaggerAppCompatActivity>()

}