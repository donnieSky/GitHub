package me.donnie.github.common.utils

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.support.annotation.CheckResult
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import org.reactivestreams.Publisher

/**
 * @author donnieSky
 * @created_at 2018/5/18.
 * @description
 * @version
 */
@CheckResult fun <T> Flowable<T>.toResult(schedulerProvider: SchedulerProvider): Flowable<Result<T>> {
    return compose { item ->
        item.map { Result.success(it) }
                .onErrorReturn { e -> Result.failure(e.message ?: "unknown", e) }
                .observeOn(schedulerProvider.ui())
                .startWith(Result.inProgress())
    }
}

@CheckResult fun <T> Observable<T>.toResult(schedulerProvider: SchedulerProvider): Observable<Result<T>> {
    return compose { item ->
        item.map { Result.success(it) }
                .onErrorReturn { e -> Result.failure(e.message ?: "unknown", e) }
                .observeOn(schedulerProvider.ui())
                .startWith(Result.inProgress())
    }
}

@CheckResult fun <T> Single<T>.toResult(schedulerProvider: SchedulerProvider): Observable<Result<T>> {
    return toObservable().toResult(schedulerProvider)
}

@CheckResult fun <T> Completable.toResult(schedulerProvider: SchedulerProvider): Observable<Result<T>> {
    return toObservable<T>().toResult(schedulerProvider)
}

fun <T> Publisher<T>.toLiveData() = LiveDataReactiveStreams.fromPublisher(this) as LiveData<T>