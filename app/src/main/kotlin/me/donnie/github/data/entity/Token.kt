package me.donnie.github.data.entity

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
data class Token(val id: Long,
                 val url: String,
                 val token: String,
                 val hashed_token: String,
                 val accessToken: String,
                 val tokenType: String)