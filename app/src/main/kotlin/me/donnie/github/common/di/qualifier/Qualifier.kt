package me.donnie.github.common.di.qualifier

import javax.inject.Qualifier

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */

@Qualifier
annotation class Logger

@Qualifier
annotation class Auth

@Qualifier
annotation class GitHub