package me.donnie.github.common.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import me.donnie.github.common.App
import me.donnie.github.common.di.qualifier.Auth
import me.donnie.github.common.di.qualifier.GitHub
import me.donnie.github.common.di.qualifier.Logger
import me.donnie.github.data.source.local.DB
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * @author donnieSky
 * @created_at 2018/5/16.
 * @description
 * @version
 */
@Module(includes = [InterceptorModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideApplicationContext(app: App): Context = app

    @Singleton
    @Provides
    fun provideDb(context: Context): DB = Room.databaseBuilder(context,
            DB::class.java, "github.db")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideOkHttpClient(@Logger interceptors: Set<Interceptor>): OkHttpClient = OkHttpClient.Builder().apply {
        interceptors.forEach {
            addNetworkInterceptor(it)
        }
    }.build()

    @Auth
    @Singleton
    @Provides
    fun provideRetrofitForAuth(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl("https://github.com/login/oauth/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()

    }

    @GitHub
    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()
    }
}