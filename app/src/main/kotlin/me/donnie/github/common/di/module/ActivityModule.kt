package me.donnie.github.common.di.module

import dagger.Module

/**
 * @author donnieSky
 * @created_at 2018/5/16.
 * @description
 * @version
 */
@Module
interface ActivityModule {
}