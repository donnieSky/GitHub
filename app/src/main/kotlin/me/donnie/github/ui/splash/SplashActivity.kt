package me.donnie.github.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import me.donnie.github.R
import me.donnie.github.common.base.BaseBindingActivity
import me.donnie.github.databinding.ActivitySplashBinding

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
class SplashActivity : BaseBindingActivity<ActivitySplashBinding, SplashViewModel>() {

    companion object {
        @JvmStatic
        fun getCallingIntent(context: Context): Intent {
            return Intent(context, SplashActivity::class.java)
        }
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_splash
    }

    override fun createViewModel(): Class<SplashViewModel> {
        return SplashViewModel::class.java
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initData() {
    }
}