package me.donnie.github.common.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import dagger.android.support.DaggerAppCompatActivity
import me.donnie.github.common.di.ViewModelFactory
import javax.inject.Inject

/**
 * @author donnieSky
 * @created_at 2018/5/11.
 * @description
 * @version
 */
abstract class BaseBindingActivity<B : ViewDataBinding,
        V : ViewModel> : DaggerAppCompatActivity() {

    @Inject lateinit var factory: ViewModelFactory

    protected val binding: B by lazy {
        createDataBinding(getLayoutResId())
    }

    protected val model: V by lazy {
        ViewModelProviders.of(this, factory).get(createViewModel())
    }

    private fun createDataBinding(@LayoutRes layoutResId: Int): B {
        return DataBindingUtil.setContentView(this, layoutResId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView(savedInstanceState)
        initData()
    }

    @LayoutRes
    protected abstract fun getLayoutResId(): Int

    protected abstract fun createViewModel(): Class<V>

    protected abstract fun initView(savedInstanceState: Bundle?)

    protected abstract fun initData()

}