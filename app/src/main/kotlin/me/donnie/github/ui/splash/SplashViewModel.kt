package me.donnie.github.ui.splash

import android.arch.lifecycle.ViewModel
import javax.inject.Inject

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
class SplashViewModel @Inject constructor() : ViewModel() {
}