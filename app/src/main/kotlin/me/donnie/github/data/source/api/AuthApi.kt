package me.donnie.github.data.source.api

import io.reactivex.Single
import me.donnie.github.data.entity.Token
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
interface AuthApi {

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("access_token")
    fun getAccessToken(@Field("code") code: String,
                       @Field("client_id") clientId: String,
                       @Field("client_secret") secret: String,
                       @Field("state") state: String,
                       @Field("redirect_url") redirect_url: String): Single<Token>

}