package me.donnie.github.ui.login

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import me.donnie.github.data.source.repository.UserRepository
import javax.inject.Inject

/**
 * @author donnieSky
 * @created_at 2018/5/18.
 * @description
 * @version
 */
class LoginViewModel @Inject constructor(private val repository: UserRepository) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

}