buildscript {
    repositories {
        jcenter()
        google()
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenLocal()
        maven(url = "https://plugins.gradle.org/m2/")
    }
}

task("clean", Delete::class) {
    delete = setOf(rootProject.buildDir)
}