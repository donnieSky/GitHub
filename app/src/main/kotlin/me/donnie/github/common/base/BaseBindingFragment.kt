package me.donnie.github.common.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import me.donnie.github.common.di.ViewModelFactory
import javax.inject.Inject

/**
 * @author donnieSky
 * @created_at 2018/5/11.
 * @description
 * @version
 */
abstract class BaseBindingFragment<B : ViewDataBinding,
        V : ViewModel> : DaggerFragment() {

    @Inject
    lateinit var factory: ViewModelFactory

    lateinit var binding: B

    lateinit var model: V

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(layoutInflater, getLayoutResId(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = ViewModelProviders.of(this, factory).get(createViewModel())
        initView(view, savedInstanceState)
        initData()
    }

    @LayoutRes
    protected abstract fun getLayoutResId(): Int

    protected abstract fun createViewModel(): Class<V>

    protected abstract fun initView(view: View, savedInstanceState: Bundle?)

    protected abstract fun initData()
}