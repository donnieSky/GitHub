package me.donnie.github.common.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerFragment

/**
 * @author donnieSky
 * @created_at 2018/5/16.
 * @description
 * @version
 */
@Subcomponent(modules = [AndroidSupportInjectionModule::class])
interface FragmentSubComponent : AndroidInjector<DaggerFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<DaggerFragment>()

}