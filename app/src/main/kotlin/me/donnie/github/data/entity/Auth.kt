package me.donnie.github.data.entity

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
data class Auth(val clientId: String,
                val clientSecret: String,
                val redirectUri: String,
                val scopes: List<String>,
                val state: String,
                val note: String,
                val noteUrl: String)