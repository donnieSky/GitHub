/**
 * @author donnieSky
 * @created_at 2018/5/11.
 * @description
 * @version
 */
object Versions {

    const val gradle = "3.2.0-alpha14"
    const val kotlin = "1.2.41"

    const val major = 0
    const val minor = 0
    const val patch = 1

    const val compileSdk = 27
    const val buildTools = "27.0.3"

    const val API_BASE_URL = "https://api.github.com/"
    const val API_OAUTH_URL = "https://github.com/login/oauth/"

    const val GITHUB_CLIENT_ID = "e0c1640074d6e23acecd"
    const val GITHUB_SECRET = "79537f81e49da27a7a219da617728a009a770ee1"
    const val REDIRECT_URL = ""

    const val applicationId = "me.donnie.github"

    const val minSdk = 19
    const val targetSdk = 27

    const val multidex = "1.0.2"
    const val databinding = "3.1.2"
    const val support = "27.1.1"
    const val constrain = "1.1.0-beta4"
    const val react_native = "+"

    const val dagger2 = "2.15"
    const val retrofit = "2.4.0"
    const val okhttp = "3.10.0"
    const val rxJava2 = "2.1.12"
    const val rxAndroid = "2.0.2"
    const val rxKotlin = "2.2.0"

    const val glide = "4.7.1"
    const val timber = "4.7.0"

    const val lifecycle = "1.1.1"
    const val room = "1.0.0"

    const val ktx = "0.3"

    const val stetho = "1.5.0"

    const val kotpref = "2.5.0"

    const val junit = "4.12"
    const val runner = "1.0.1"
    const val espresso = "3.0.1"

}