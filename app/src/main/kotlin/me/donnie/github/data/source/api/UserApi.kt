package me.donnie.github.data.source.api

import io.reactivex.Single
import me.donnie.github.data.entity.Auth
import me.donnie.github.data.entity.Token
import me.donnie.github.data.entity.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
interface UserApi {

    @POST("authorizations")
    fun login(@Body auth: Auth): Single<Token>

    @GET("user")
    fun getUser(): Single<User>

}