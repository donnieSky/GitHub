package me.donnie.github.common.di.module

import dagger.Module
import dagger.Provides
import me.donnie.github.data.source.local.DB
import me.donnie.github.data.source.local.dao.UserDao
import javax.inject.Singleton

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
@Module
class DaoModule {

    @Singleton
    @Provides
    fun provideUserDao(db: DB): UserDao = db.userDao()

}