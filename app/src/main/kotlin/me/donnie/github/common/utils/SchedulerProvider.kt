package me.donnie.github.common.utils

import io.reactivex.Scheduler

/**
 * @author donnieSky
 * @created_at 2018/5/17.
 * @description
 * @version
 */
interface SchedulerProvider {

    fun ui(): Scheduler

    fun computation(): Scheduler

    fun newThread(): Scheduler

    fun io(): Scheduler

}